/**
 * Budget Controller
 * Handles calculation and data manipulation   
 */

var budgetController = (function(uiCtr){

    var data = {
        allItems: {
            exp:[],
            inc: []
        },
        totals: {
            inc: 0,
            exp: 0,
            budget: 0,
            perc: -1
        } 
    };

    var income = function(id, description, value) {
        this.description = description;
        this.value = value;
        this.id = id;
    };

    var expense = function(id, description, value) {
        this.description = description;
        this.value = value;
        this.id = id;
        this.perc = -1;
    };

    expense.prototype.calcPerc = function() {
        if (data.totals.inc > 0) {
            this.perc =  Math.round((Math.abs(this.value) / data.totals.inc) * 100);
        } else {
            this.perc = -1;
        }
    };

    expense.prototype.getPerc = function() {
        return this.perc;
    };

    var getSum = function(type) {

        var value, input;

        value = 0;
        input = data.totals[type];

        if (data.allItems[type].length > 0) {

            data.allItems[type].forEach(function(key) {
                value += key.value;
            });
        } 
        data.totals[type] = value;
    };

    return {

        addItem: function(des, value, type) {
            var newItem, ID;

            // Assign IDs based on the last item ID in the array of all items [type]
            if( data.allItems[type].length > 0 ) {
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
            } else {
                ID = 0;
            }
            // Create new item based on which type is the input
            if( type === 'exp' ) {
                newItem = new expense( ID, des, value);
            } else if ( type === 'inc' ) {
                newItem = new income( ID, des, value);
            }

            // Add new item to the data variable based on type
            data.allItems[type].push(newItem);

            // return new item
            return newItem;
        },

        calculateBudget: function() {

            getSum('inc');
            getSum('exp');
            
            data.totals.budget = data.totals.inc - data.totals.exp;
            
            if (data.totals.budget > 0) {
                data.totals.perc = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.totals.perc = -1;
            }

        },

        calculatePercentages: function() {
            if (data.allItems.exp.length !== -1) {
                data.allItems.exp.forEach(function(key){
                    key.calcPerc();
                });
            }
        },

        removeItem: function(id, type) {
            var ids,index;

            ids = data.allItems[type].map(function(current){
                return current.id
            });

            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems[type].splice(index, 1);
            }

        },

        getBudgetTotals: function() {
            return data.totals;
        },

        getPercentages: function() {
            var allPerc = data.allItems.exp.map(function(key){
                return key.getPerc();
            });

            return allPerc;
        },

        testing: function() {
            return data;
        }

    }

})(uiController);


/**
 * UI Controller
 * Handles visual interpretation of user actions from
 * app controller and data injection from budget controller    
 */

var uiController = (function(budgetCtr){
    
    var DOMstrings = {      // all essential string selectors
            type: '.add__type',
            description: '.add__description',
            value: '.add__value',
            sum_income_value: '.budget__income--value',
            sum_expense_value: '.budget__expenses--value',
            perc_expense_value: '.budget__expenses--percentage',
            budget_value: '.budget__value',
            income_list_class: '.income__list',
            expense_list_class: '.expenses__list',
            expense_item_percentage: '.item__percentage',
            container: '.container',
            dateLabel: '.budget__title--month'
        };


    var formatNumber = function(num, type) {
        num = Math.abs(num);
        num = num.toFixed(2);

        numSplit = num.split('.');

        int = numSplit[0];

        if (int.length > 3) {
            int = int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, 3);
        }

        dec = numSplit[1];

        return (type === 'inc' ? '+ ' : '- ' ) + int + '.' + dec; 
    }

    /************ Public methods ***********/ 

    return {

        displayDate: function(){
            var date, months, month, year;

            date = new Date();

            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'December'];
            month = date.getMonth();
            year = date.getFullYear();

            document.querySelector(DOMstrings.dateLabel).textContent = months[month] + ' ' + year;
        },

        getInput: function() {
            return {
                type: document.querySelector(DOMstrings.type).value,
                description: document.querySelector(DOMstrings.description).value,
                val: parseFloat(document.querySelector(DOMstrings.value).value)
            }
        },

        addNewListItem: function(item, type) {
            var html, htmlNew, element;

            if ( type === 'inc' ) {

                element = DOMstrings.income_list_class;
                html = '<div class="item clearfix" id="inc-%id%"> <div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';

            } else if ( type === 'exp' ) {

                element = DOMstrings.expense_list_class;
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';

            }

            newHtml = html.replace('%id%', item.id);
            newHtml = newHtml.replace('%description%', item.description);
            newHtml = newHtml.replace('%value%', formatNumber(item.value, type));

            // Insert HTML to the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        showItemPercentage: function() {
            var el_perc;
            
            el_perc = document.querySelectorAll(DOMstrings.expense_item_percentage);

            if (el_perc !== -1) {
                el_perc.forEach(function(key, index){

                    key.textContent = budgetCtr.getPercentages()[index] !== -1 ? 
                        budgetCtr.getPercentages()[index]+ '%' :
                            key.textContent = '---';
                    
                });
            }
        },

        removeListItem: function(selectorID) {
            var el = document.getElementById(selectorID);
            el.parentNode.removeChild(el);
        },

        clearInputFields: function() {
            var inputFields, inputFieldsArr;

            inputFields = document.querySelectorAll(DOMstrings.description + ',' + DOMstrings.value);
            inputFieldsArr = Array.prototype.slice.call(inputFields);

            inputFieldsArr.forEach(function(key, index){
                key.value = '';
            });

            inputFieldsArr[0].focus();
        },

        resetBudgetView: function(obj) {
            var show_income_value = document.querySelector(DOMstrings.sum_income_value),
                show_expense_value = document.querySelector(DOMstrings.sum_expense_value),
                show_perc_expense_value = document.querySelector(DOMstrings.perc_expense_value),
                show_budget_value = document.querySelector(DOMstrings.budget_value),
                type;

            (obj.budget > 0) ? type = 'inc' : type = 'exp';
            
            show_income_value.textContent = formatNumber( obj.inc, 'inc' );
            show_expense_value.textContent = formatNumber( obj.exp, 'exp' );
            show_budget_value.textContent = formatNumber(obj.budget, type);

            if (obj.perc > 0) {
                show_perc_expense_value.textContent = obj.perc + '%';
            } else {
                show_perc_expense_value.textContent = '---';
            }
        },

        toggleTypeClass: function() {
            var inputs = document.querySelectorAll(DOMstrings.description + ',' + DOMstrings.value + ',' + DOMstrings.type);

            if (this.value === 'exp') {
                if (this.classList.contains('red-focus')) return;
                inputs.forEach(function(key){
                    key.classList.add('red-focus');
                });
            } else {
                inputs.forEach(function(key){
                    key.classList.remove('red-focus');
                });
            }
        },

        getDOMstrings: function() {
            return DOMstrings;
        }
    }
    
})(budgetController);


/**
 * Controller service
 * Accepts user input and serves methods from other modules.   
 */

var controllerService = (function(uiCtr, budgetCtr){   

    var setupEventListeners = function() {
        var DOMstrings = uiCtr.getDOMstrings();
        // Get field input (onclick, onKeyPress)
        document.querySelector('.add__btn').addEventListener('click', ctrlAddItem);
        document.addEventListener('keypress', function(e){
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });

        document.querySelector(DOMstrings.container).addEventListener('click', ctrlRemoveItem);
        document.querySelector(DOMstrings.type).addEventListener('change', uiCtr.toggleTypeClass);
    };

    var updateBudget = function() {
        
        budgetCtr.calculateBudget();
        uiCtr.resetBudgetView(budgetCtr.getBudgetTotals());

    };

    var updatePercentages = function() {

        budgetCtr.calculatePercentages();
        uiCtr.showItemPercentage();

    };

    var ctrlAddItem = function() {
        var input, newItem;

        input = uiCtr.getInput();
        
        if (input.description !== "" && !isNaN(input.val) && input.val > 0 ) {

            // Add new item to budget data var
            newItem = budgetCtr.addItem(input.description, input.val, input.type);
            
            // UiController adds new list item to the DOM
            uiCtr.addNewListItem(newItem, input.type);
            
            // Clear inputs
            uiCtr.clearInputFields();

            // Calculate and Update Budget
            updateBudget()
            
            // Calculate and update Percentage 
            updatePercentages();

        }

        return newItem;
    };

    var ctrlRemoveItem = function(event) {
        var type, itemID, splitID, ID;

        // type = itemID.getAttribute('id').split('-')[0];

        itemID = _parents(event.target, 'item').id;

        if (itemID) {

            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);

            // Remove item from data var
            budgetCtr.removeItem(ID, type)
            // Remove item from the DOM
            uiCtr.removeListItem(itemID);

            // Calculate and Update Budget
            updateBudget()
            
            // Calculate and update Percentage 
            updatePercentages();
        } 
        
    };

    var _parents = function(el, className) {
        var output;

        function iterate(element, className) {
          if ( element.classList.contains(className) )  {

            output = element;

          } else {
            iterate(element.parentNode, className);
          }
        }
        iterate(el, className);

        return output;
    }

    
    return {

        init: function() {
            // Reset all visually interpreted values
            uiCtr.resetBudgetView({
                inc: 0,
                exp: 0,
                perc: 0,
                budget: 0.00
            });
            
            // Setup Event listeners
            setupEventListeners();
            uiCtr.displayDate();
        }

    }

})(uiController, budgetController);

controllerService.init();